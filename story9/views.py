from django.shortcuts import render
from django.http import HttpResponseRedirect
from social_django.models import *
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.forms.models import model_to_dict
import requests
import json

# Create your views here.
def home(request):
    def to_serializer(val):
	    return str(val)

    if 'user' not in request.session and str(request.user) != 'AnonymousUser':
	    data = json.dumps(model_to_dict(request.user), default=to_serializer)
	    request.session['user'] = {}
	    request.session['user'][request.user.get_username()] = json.loads(data) #this

    content = {'title' : 'Namamu?', 'name' : request.user.get_username()}
    return render(request, "wishlist.html", content)

def laplist_v(request):
    return render(request, "wishlist.html")

def logout(request):
	request.session.flush()     #this
	content = {'title' : 'Welcome!', 'name' : request.user.get_username()}
	return render(request, 'wishlist.html', content)