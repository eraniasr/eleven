from django.urls import include, path
from .views import *

urlpatterns = [
    path('', home, name='home'),
    path('laplist/', laplist_v, name='laplist'),
    path('logout/', logout, name='logout'),
    path('auth/', include('social_django.urls', namespace='social')),   #this
]